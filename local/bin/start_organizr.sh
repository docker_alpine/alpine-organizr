#!/bin/sh

set -e

cd /conf.d/organizr

exec /usr/bin/php83 -S 0.0.0.0:8080 -t /app/Organizr

